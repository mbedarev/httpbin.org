import logging
import datetime
import allure

logging.basicConfig(level=logging.INFO)
logg = logging.getLogger()


def log(message, attach='attachment'):
    '''
    Функция логирования. Выводит сообщение в лог и дублирует его в allure-отчет
    :param message: Заголовок сообщения
    :param attach: содержательная часть
    '''
    attach = str(attach)
    allure.attach(name=message, body=attach, attachment_type=allure.attachment_type.TEXT)
    dtime = datetime.datetime.now().strftime('%H:%M:%S')
    if attach == 'attachment':
        logg.info(dtime + ' ' + str(message))
    else:
        logg.info(dtime + ' ' + message + ' ' + attach)
