import pytest
import requests
from utils.base_http_bin_org import BaseHttp
import allure

from utils.logging import log


class TestBeeline:

    @allure.story('Request Inspection: headers')
    def test_headers(self):
        """Return the incoming request's HTTP headers."""
        log(f'make request {BaseHttp.BASE_URL}' + 'headers')
        res = requests.get(f'{BaseHttp.BASE_URL}' + 'headers')
        log(res)
        with allure.step('Проверяем статус код результата'):
            assert res.status_code == 200
        log(res.text)

    @allure.story('Status codes')
    @pytest.mark.parametrize(argnames='codes',
                             argvalues=[200, 300, 400, 500, 100],
                             ids=['	Success', 'Redirection', 'Client Errors', 'Server Errors',
                                  'Informational responses'])
    def test_get_status_codes(self, codes):
        '''Generates responses with given status code'''
        log(f'make request {BaseHttp.BASE_URL}' + f'status/{codes}')
        res = requests.get(f'{BaseHttp.BASE_URL}' + f'status/{codes}')
        log(res)
        with allure.step('Проверяем статус код результата'):
            assert res.status_code == codes
        log(res.text)

    @allure.story('redirect')
    def test_redirect(self):
        '''redirect n times'''
        n = 3
        log(f'make request {BaseHttp.BASE_URL}' + f'redirect/{n}')
        res = requests.get(f'{BaseHttp.BASE_URL}' + f'redirect/{n}')
        log(res)
        with allure.step('Проверяем статус код результата'):
            assert res.status_code == 200
        log(res.text)
